#Get Started

Move the template file here:
~/.git-commit-template

Configure you default editor
git config --global core.editor "vim"

Setup commit template
git config --global commit.template ~/.git-commit-template

Now commit with
git commit
